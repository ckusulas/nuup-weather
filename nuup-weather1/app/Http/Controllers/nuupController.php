<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \GuzzleHttp\Client;


class nuupController extends Controller
{
    //


    public function getWeather_API(){

    	$client = new client();

    	$country = env('COUNTRY_WEATHER_APP');
    	$city = env('CITY_WEATHER_APP');
    	$lang = env('LANG_WEATHER_APP');
    	$units = env('UNITS_WEATHER_APP');
    	$appid = env('APPID_WEATHER');
 
    


    	$url = "http://api.openweathermap.org/data/2.5/forecast?q=$country,$city&lang=$lang&units=$units&appid=$appid";
 

    	$request = $client->get($url);
    	



    	if($request->getStatusCode()==200){

    		$body = $request->getBody();
    		$data = json_decode($request->getBody());

            
              return view('forecast', ["data" => $data]);
    	}
    	else{
    		echo "Error en respuesta del API, revisar el comando de solicitud";
    	}

      
    }




    public function getWeather_API2(){

        $client = new client();

        $country = env('COUNTRY_WEATHER_APP');
        $city = env('CITY_WEATHER_APP');
        $lang = env('LANG_WEATHER_APP');
        $units = env('UNITS_WEATHER_APP');
        $appid = env('APPID_WEATHER');
 
    


        $url = "http://api.openweathermap.org/data/2.5/forecast?q=$country,$city&lang=$lang&units=$units&appid=$appid";
 

        $request = $client->get($url);
        



        if($request->getStatusCode()==200){

            $body = $request->getBody();
            $data = json_decode($request->getBody());

            
              return view('forecast2', ["data" => $data]);
        }
        else{
            echo "Error en respuesta del API, revisar el comando de solicitud";
        }

      
    }
}
