
<head>
<link rel="stylesheet" href="css/app.css">
<link rel="stylesheet" href="css/weather-icons.min.css">

</head>

 

<body>
    <script src="js/app.js" charset="utf-8"></script>


<div class="container">
  <div class="row">
    <div class="col-md-6">

<table class="table" >
      <thead>
      <tr>
        
        <th scope="col">Dia</th>     
        <th scope="col">Cielo</th>
        <th scope="col">Clima</th>
        <th scope="col">Temp</th>
      </tr>
      </thead>
      <tbody>

<?php
//print_r($data);

    
    function skyInterpret($opt){

      switch($opt){

        case "Clouds":
          return "Nubes";

        case "Clear":
          return "Despejado";

      }
    }


      function getImgInterpret($opt){

      //iconos de referencia - examples
      //https://erikflowers.github.io/weather-icons/
        
        switch($opt){
          case "muy nuboso":
            return "wi wi-fog";

          case "nubes dispersas":
            return "wi wi-cloudy-windy";

          case "algo de nubes":
            return "wi wi-cloudy-windy";

          case "cielo claro":
            return "wi wi-day-cloudy";

          case "nubes":
            return "wi  wi-cloudy";   

        }
      }



      function translateDay($day){
       
        switch($day){
          case "Monday":
            return "Lunes";

          case "Tuesday":
            return "Martes";

          case "Wednesday":
            return "Miercoles";

          case "Thurdsay": 
            return "Jueves";

          case "Friday":
            return "Viernes";  

          case "Saturday":
            return "Sabado";  

          case "Sunday":
            return "Domingo";  

        }
      }



     foreach($data->list as $row){

                
                if(date("G:i", strtotime($row->dt_txt))=="12:00"){
                    

                    echo "<tr><td>" . substr(translateDay(date("l", strtotime($row->dt_txt))),0,3) . "</td>";
                
         
            //      echo "<td>" . date("G:i", strtotime($row->dt_txt)) . "</td>";
                     
                    foreach($row->weather as $elem){
                    
                        echo "<td>" . skyInterpret($elem->main) . "</td>";
                        echo "<td><i class='".getImgInterpret($elem->description)."' title='$elem->description'></i></td>";
                    }

                    echo "<td>" . $row->main->temp . " °C<td></tr>";

                }
                 
               
            }

 

?>


</tbody>
</table>

</div>
</div>
</div>


</body>


 