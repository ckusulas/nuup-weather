
<head>
<link rel="stylesheet" href="css/app.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/weather-icons.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css">

</head>


<body>
  <script src="js/app.js" charset="utf-8"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" charset="utf-8"></script>

  
 
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="row container d-flex justify-content-center">
            <div class="col-lg-4 grid-margin stretch-card">
                <!--weather card-->
                <div class="card card-weather">
                    <div class="card-body">
                        <div class="weather-date-location">
                            <h3>Viernes</h3>
                            <p class="text-gray"> <span class="weather-date">29 Enero 2021</span> <span class="weather-location">CDMX, México</span> </p>
                        </div>
                        <div class="weather-data d-flex">
                            <div class="mr-auto">
                                <h4 class="display-3">20 <span class="symbol">°</span>C</h4>
                                <p> Nublado </p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="d-flex weakly-weather">
<?php

    function skyInterpret($opt){

      switch($opt){

        case "Clouds":
          return "Nubes";

        case "Clear":
          return "Despejado";

      }
    }


      function getImgInterpret($opt){

      //iconos de referencia - examples
      //https://erikflowers.github.io/weather-icons/
        
        switch($opt){
          case "muy nuboso":
            return "wi wi-fog";

          case "nubes dispersas":
            return "wi wi-cloudy-windy";

          case "algo de nubes":
            return "wi wi-cloudy-windy";

          case "cielo claro":
            return "wi wi-day-cloudy";

          case "nubes":
            return "wi  wi-cloudy";   

        }
      }



      function translateDay($day){
       
        switch($day){
          case "Monday":
            return "Lunes";

          case "Tuesday":
            return "Martes";

          case "Wednesday":
            return "Miercoles";

          case "Thurdsay": 
            return "Jueves";

          case "Friday":
            return "Viernes";  

          case "Saturday":
            return "Sabado";  

          case "Sunday":
            return "Domingo";  

        }
      }



           foreach($data->list as $row){

                
                    if(date("G:i", strtotime($row->dt_txt))=="12:00"){
                        

                     //   echo "<tr><td>" . substr(translateDay(date("l", strtotime($row->dt_txt))),0,3) . "</td>";
                    
             
                //      echo "<td>" . date("G:i", strtotime($row->dt_txt)) . "</td>";
                         
                   

                     
                        echo "<div class='weakly-weather-item'>";
                        echo  "<p class='mb-0'>". substr(translateDay(date("l", strtotime($row->dt_txt))),0,3) ." </p>";
                        foreach($row->weather as $elem){                                               
                            echo "<i class='".getImgInterpret($elem->description)."' title='$elem->description'></i>";
                        }
                         
                         echo "<p class='mb-0'> ".$row->main->temp."</p></div>";

                    }
                 
               
            }

        ?>
      </div>
                     
                           
                        
                       
                      
                    </div>
                </div>
                <!--weather card ends-->
            </div>
        </div>
    </div>
</div>
 


 </body>